import os.path
import time
import json
import subprocess
import requests

request_path = "./route_request_sms"

while True:
    while not os.path.exists(request_path):
        time.sleep(1)
    for sms_request in os.listdir(request_path):
        with open(os.path.join(request_path,sms_request)) as data_file:
            data = json.load(data_file)
            origin = data["from"]
            destination = data["to"]
            response = (requests.get("http://localhost:3002/api/recommendation/"+origin+"/"+destination)).json()
            response = json.dumps(response['bestRoute'])
            subprocess.call(["php", "-f", "send_sms.php", data["number"], response])
        os.remove(os.path.join(request_path, sms_request))
